'use strict';

var expect = require('expect.js');
var Browser = require('zombie');
var browser = new Browser();

var constants = require('../../config/constants');

describe('User visits home page', () => {

    before((done) => {
        browser.visit(constants.BASE_URL, done);
    });

    it('should contain the jumbotron', () => {
        browser.assert.success();
        browser.assert.text('title', 'DayLogApp');
        expect(browser.assert.element('.jumbotron')).to.exist;
    });

    describe('User clicks "See More" link', () => {
        before((done) => {
            browser.clickLink('.jumbotron a', done);
        });

        it('should go to /daylog', () => {
            browser.assert.success();
            expect(browser.assert.text('.card-header', 'Day Log')).to.be.true;
        });
    });

    after((done) => {
        done();
    });
});