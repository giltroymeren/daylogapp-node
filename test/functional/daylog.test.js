'use strict';

// TODO: Check if all test requires can be inserted into one init file
var expect = require('expect.js');
var Browser = require('zombie');
var browser = new Browser();
var mongoose = require('mongoose');

var DayLogModel = require('../../model/daylog.model');
var constants = require('../../config/constants');

const BASE_URL = constants.BASE_URL + '/daylog';
const DAYLOG = new DayLogModel({
    "title" : "Lorem ipsum dolor sit amet",
    "slug" : "lorem-ipsum",
    "location" : "Nec congue lorem nulla id ligula",
    "logAt" : Date.now(),
    "category" : "Major"
});

describe('User visits /daylog', () => {
    before(() => {
        /**
         * TODO: Resolve redundant connection to testing while running app with environment
         *  OK: `NODE_ENV=testing npm start` and start test
         *  NOK: `npm start` (defaults to prod DB) and start test
         *      create DayLog TC fails
         */
        mongoose.connect(constants.DATABASE.testing);
    });

    beforeEach((done) => {
        browser.visit(BASE_URL, done);
    });

    beforeEach(() => {
        browser.assert.success();
        browser.assert.text('title', 'DayLogApp');
        expect(browser.assert.text('.card-header', 'Day Log')).to.be.true;
    });

    it('should show an alert when no DayLogs are available', () => {
        expect(browser.assert.element('.alert-info')).to.exist;
    });

    describe('Insert one Day Log in database', () => {
        before((done) => {
            DayLogModel.create(DAYLOG);
            browser.visit(BASE_URL, done);
        });

        it('should show the Day Log in a table when it is added', () => {
            browser.assert.success();
            expect(browser.assert.element('.card')).to.exist;
            expect(browser.assert.text('.card-header', 'Day Log')).to.be.true;
            expect(browser.assert.element('table')).to.exist;
        });
    });

    describe('Clicks "Create Day Log" button', () => {
        beforeEach((done) => {
            browser.clickLink('.card-footer .btn', done);
        });

        it('should go to /daylog/create', () => {
            browser.assert.success();
            browser.assert.url(BASE_URL + '/create');
            expect(browser.assert.element('form')).to.exist;
        });
    });

    afterEach((done) => {
        mongoose.connection.dropDatabase();
        done();
    });
});

describe('User visits /daylog/create', () => {
    before((done) => {
        browser.visit(BASE_URL + '/create', done);
    });

    describe('Clicks submit button with empty form', () => {
        beforeEach((done) => {
            browser.pressButton('button[type="submit"]', done);
        });

        it('should show 5 "required" error messages near the 5 fields', () => {
            browser.assert.elements('form .alert-danger', 5);
            browser.assert.text('form .alert-danger', new RegExp('required$'));
        });
    });

    describe('Clicks submit button with valid form', () => {
        before((done) => {
            browser.fill('title', DAYLOG.title);
            browser.fill('input[name="slug"]', DAYLOG.slug);
            browser.fill('input[name="location"]', DAYLOG.location);
            browser.fill('input[name="logAt"]', DAYLOG.logAt);
            browser.choose('input[name="category"][value="' + DAYLOG.category + '"]', DAYLOG.category);
            browser.pressButton('button[type="submit"]', done);
        });

        it('should show the DayLog view page', () => {
            browser.assert.success();
            browser.assert.url(BASE_URL + '/' + DAYLOG.slug);
        });

        it('should show the view form with the submitted values', () => {
            assertFormHasCreatedDayLogValues();
        });

        it('should be in the DayLog home page table', () => {
            browser.clickLink('.card-footer .btn', () => {
                browser.assert.text('.card-header', 'Day Log');
                browser.assert.text('table', DAYLOG.title);
            });
        });
    });

    afterEach((done) => {
        mongoose.connection.dropDatabase();
        done();
    });
});

describe('Clicks "' + DAYLOG.title + '"', () => {
    it('should show the view form with the Day Log\'s values in readonly or disabled fields', () => {
        DayLogModel.create(DAYLOG);
        browser.visit(BASE_URL, () => {
            browser.clickLink('a[href="/daylog/' + DAYLOG.slug + '"]', () => {
                browser.assert.success();
                browser.assert.url(BASE_URL + '/' + DAYLOG.slug);
                expect(browser.assert.text('.card-header', 'View Day Log')).to.be.true;

                assertFormHasCreatedDayLogValues();

                browser.assert.element('textarea[name="title"][readonly]');
                browser.assert.element('input[name="slug"][readonly]');
                browser.assert.element('input[name="location"][readonly]');
                browser.assert.element('input[name="logAt"][readonly]');
                browser.assert.elements('input[name="category"][disabled]', 3);
            });
        });
    });

    afterEach((done) => {
        mongoose.connection.dropDatabase();
        done();
    });
});

function assertFormHasCreatedDayLogValues() {
    browser.assert.text('textarea[name="title"]', DAYLOG.title);
    expect(browser.field('input[name="slug"]', DAYLOG.slug)).to.be.true;
    expect(browser.field('input[name="location"]', DAYLOG.location)).to.be.true;
    expect(browser.field('input[name="logAt"]', DAYLOG.logAt)).to.be.true;
    browser.assert.element('input[name="category"][value="' + DAYLOG.category
        + '"]:checked');
}
