'use strict';

var DayLogModel = require('../model/daylog.model');
var constants = require('../config/constants');

var DayLogController = {
    getCustomExpressValidators: {
        customValidators: {
            isSlugValid(req, res, next, slug) {
                DayLogModel.findOne({ slug: slug },
                    (err, daylog) => {
                        if (err || daylog === null) {
                            console.error('DayLog with slug <' + slug + '> was not found');
                            res.status(404)
                            var err = new Error('Not Found');
                            err.status = 404;

                            res.format({
                                html: () => { next(err); },
                                json: () => { res.json({message : err.status  + ' ' + err}); }
                            });
                        } else {
                            req.slug = slug;
                            next();
                        }
                    }
                )
            },

            isSlugUnique(slug, operation) {
                return new Promise((resolve, reject) => {
                    DayLogModel.findOne({ slug: slug }, (err, daylog) => {
                        if(err) throw err;

                        if(daylog === null) {
                            resolve();
                        } else if(slug === daylog.slug &&
                            operation === constants.FORM_OPERATIONS.edit) {
                            resolve();
                        } else {
                            reject();
                        }
                    })
                })
            },

        }
    },

    isUserLoggedIn: (request, response, next) => {
        if (!request.isAuthenticated()) {
            response.redirect('/user/login');
        } else {
            return next();
        }
    },

    index: (request, response) => {
        DayLogModel.find({ user: request.user },
            (err, daylogs) => {
                if(err) return console.error(err);

                return response.format({
                    html: () => {
                        response.render('daylog/index', {
                            daylogs: daylogs,
                            // NOTE: Defined in showHomeIfNonUserAccessesDayLog()
                            message: request.flash('message'),
                            type: request.flash('type')
                        });
                    },
                    json: () => { response.json(daylogs); }
                })
            }
        );
    },

    create: (req, res) => {
        res.render('daylog/form',
            getFormPageConfig(constants.FORM_OPERATIONS.create));
    },

    store: (req, res) => {
        validateDayLog(req);
        var newDayLog = getDayLogFromRequest(req);

        req.asyncValidationErrors()
            .then(() => {
                DayLogModel.create(newDayLog,
                    (err, daylog) => {
                        if(err)  return res.send("There was a problem adding the information to the database: " + err);

                        console.log('Created: "' + daylog.title + '"');

                        return res.format({
                            html: () => {
                                res.redirect("/daylog/" + daylog.slug);
                            },
                            json: () => { res.json(daylog); }
                        }
                    );
                });
            })
            .catch((errors) => {
                return res.format({
                    html: () => {
                        res.render('daylog/form', Object.assign({
                                daylog: newDayLog,
                                errors: errors
                            }, getFormPageConfig(constants.FORM_OPERATIONS.create))
                        );
                    }
                });
            });
    },

    show: (req, res) => {
        // TODO: Find other way to get slug instead of using arbitrary array splitting
        DayLogModel.findOne({ slug: req.url.split("/")[1], user: req.user },
            (err, daylog) => {
                if(daylog === null) return showHomeIfNonUserAccessesDayLog(req, res, "view");

                if(err) console.error('GET Error: There was a problem retrieving: ' + err);

                console.info('Show: "' + daylog.title + '"');

                res.format({
                    html: () => {
                        res.render('daylog/form', Object.assign({
                                daylog: daylog
                            }, getFormPageConfig(constants.FORM_OPERATIONS.view)));
                    },
                    json: () => { res.json(daylog); }
                });
            }
        );
    },

    edit: (req, res) => {
        DayLogModel.findOne({ slug: req.url.split("/")[1], user: req.user },
            (err, daylog) => {
                if(daylog === null) return showHomeIfNonUserAccessesDayLog(req, res, "edit");

                if(err) return console.error('GET Error: There was a problem retrieving: ' + err);

                res.format({
                    html: () => {
                        res.render('daylog/form', Object.assign({
                                daylog : daylog
                            }, getFormPageConfig(constants.FORM_OPERATIONS.edit, daylog))
                        );
                    },
                    json: () => { res.json(daylog); }
                });
            }
        );
    },

    update: (req, res) => {
        validateDayLog(req, constants.FORM_OPERATIONS.edit);
        var newDayLog = getDayLogFromRequest(req);

        req.asyncValidationErrors()
            .then(() => {
                DayLogModel.findOne({ slug: newDayLog.slug, user: req.user },
                    (err, daylog) => {
                        if(daylog === null) return showHomeIfNonUserAccessesDayLog(req, res, "edit");

                        daylog.update(newDayLog, (err, slug) => {
                            if(err) res.send("There was a problem updating the information to the database: " + err);

                            console.info('Editing: "' + daylog.title + '"');

                            res.format({
                                html: () => { res.redirect("/daylog/" + daylog.slug); },
                                json: () => { res.json(daylog); }
                            });
                        })
                    }
                );
            })
            .catch((errors) => {
                return res.format({
                    html: () => {
                        res.render('daylog/form', Object.assign({
                                daylog: newDayLog,
                                errors: errors
                            }, getFormPageConfig(constants.FORM_OPERATIONS.edit, newDayLog))
                        );
                    }
                });
            });
    },

    delete: (req, res) => {
        DayLogModel.findOne({ slug: req.url.split("/")[1], user: req.user },
            (err, daylog) => {
                if(daylog === null) return showHomeIfNonUserAccessesDayLog(req, res, "delete");

                if(err) return console.error(err);

                daylog.remove((err, daylog) => {
                    if(err) return console.error(err);

                    console.info('Deleting: "' + daylog.title + '"');

                    res.format({
                        html: () => { res.redirect("/daylog"); }
                    });
                });
            }
        );
    }
};

function getFormPageConfig(operation, daylog = null) {
    var config = {
        operation: operation,
        constants: constants,
        method: constants.HTTP_METHODS.post
    }

    switch (operation) {
        case constants.FORM_OPERATIONS.create:
            config.action = "store";
            break;
        case constants.FORM_OPERATIONS.view:
            config.action = null;
            config.method = null;
            break;
        case constants.FORM_OPERATIONS.edit:
            config.action = '/daylog/' + daylog.slug + '/edit';
            break;
    }

    return config;
}

function validateDayLog(request, operation = null) {
    request.assert('title', 'Title is required').notEmpty();

    request.assert('slug', 'Slug is required').notEmpty();
    request.assert('slug', 'Slug is already in use').isSlugUnique(operation);

    request.assert('location', 'Location is required').notEmpty();
    request.assert('logAt', 'Log date is required').notEmpty();
    request.assert('category', 'Category is required').notEmpty();
}

function getDayLogFromRequest(request) {
    return {
        title : request.body.title,
        slug : request.body.slug,
        location : request.body.location,
        logAt : (request.body.logAt) ? new Date(request.body.logAt) : '',
        category : request.body.category,
        user: request.user
    };
}

function showHomeIfNonUserAccessesDayLog(request, response, method) {
    var message = "Cannot " + method + " daylog not under your account.";
    console.log(message);
    request.flash('message', message);
    request.flash('type', 'alert-warning');
    response.redirect('/daylog');
}

module.exports = DayLogController;
