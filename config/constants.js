'use strict';

module.exports = Object.freeze({
    DATABASE: {
        production: 'mongodb://localhost/daylogapp',
        testing: 'mongodb://localhost/daylogapp-test'
    },
    ENVIRONMENT: {
        production: 'production',
        testing: 'testing'
    },
    FORM_OPERATIONS: {
        create: 'Create',
        view: 'View',
        edit: 'Edit'
    },
    HTTP_METHODS: {
        post: 'post'
    },
    BASE_URL: 'http://localhost:3000'
});
