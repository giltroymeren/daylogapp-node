var LocalStrategy = require('passport-local').Strategy;
var UserModel = require('../model/user.model');

const localStrategy = {
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true,
};

module.exports = (passport) => {
    passport.serializeUser((user, done) => {
        done(null, user.id);
    });

    passport.deserializeUser((id, done) => {
        UserModel.findById(id, (error, user) => {
            done(error, user);
        });
    });

    passport.use('register',
        new LocalStrategy(localStrategy,
            (request, username, password, done) => {
                process.nextTick(() => {
                    UserModel.findOne({ username:  username },
                        (error, user) => {
                            if (error) return done(error);

                            if (user) {
                                return done(null, false,
                                    request.flash('error-register',
                                        'Username "' + username + '" is already in use.'));
                            } else {
                                var newUser = new UserModel();
                                newUser.username = username;
                                newUser.password = newUser.generateHash(password);
                                newUser.save((error) => {
                                    if (error) throw error;

                                    return done(null, newUser);
                                });
                            }
                        }
                    );
                });
        }
    ));

    passport.use('login',
        new LocalStrategy(localStrategy,
            (request, username, password, done) => {
                UserModel.findOne({ username:  username },
                    (error, user) => {
                        if (error) throw error;

                        if (!user)  {
                            return done(null, false,
                                request.flash('error-login',
                                    'No username "' + username + '" found.'));
                        }

                        if (!user.validPassword(password)) {
                            return done(null, false,
                                request.flash('error-login',
                                    'Wrong username/password combination.'));
                        }

                        return done(null, user);
                    }
                );
            }
        )
    );
};
