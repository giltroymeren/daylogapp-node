'use strict';

var mongoose = require('mongoose');
var constants = require('./constants');

var databaseURL = constants.DATABASE.production;

if(process.env.NODE_ENV === constants.ENVIRONMENT.testing) {
    databaseURL = constants.DATABASE.testing;
}

mongoose.connect(databaseURL);
mongoose.connection.on('connection', () => {
    console.log('Mongoose default connection open to: ' + databaseURL);
});

module.exports = mongoose;
