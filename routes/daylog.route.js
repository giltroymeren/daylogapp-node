'use strict';

var express = require('express');
var expressValidator = require('express-validator');
var router = express.Router();
var database = require('../config/database');

var DayLogModel = require('../model/daylog.model');
var DayLogController = require('../controller/daylog.controller');

router.use(expressValidator(DayLogController.getCustomExpressValidators));

router.use(DayLogController.isUserLoggedIn);

router.param('slug', DayLogController.getCustomExpressValidators.customValidators.isSlugValid);

router.get('/', DayLogController.index);

router.get('/create', DayLogController.create);

router.post('/store', DayLogController.store);

router.get('/:slug', DayLogController.show);

router.get('/:slug/edit', DayLogController.edit);

router.put('/:slug/edit', DayLogController.update);

router.delete('/:slug/delete', DayLogController.delete);

module.exports = router;
