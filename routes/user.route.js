'use strict';

var express = require('express');
var router = express.Router();
var database = require('../config/database');

var UserController = require('../controller/user.controller');

router.get('/', UserController.index);

router.get('/register', UserController.showRegister);

router.post('/register', UserController.register);

router.get('/login', UserController.showLogin);

router.post('/login', UserController.login);

router.get('/logout', UserController.logout);

module.exports = router;
