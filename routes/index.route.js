'use strict';

var express = require('express');
var router = express.Router();

router.use((req, res, next) => {
    if (req.isAuthenticated()) res.locals.loggedIn = true;

    return next();
});

router.get('/', (req, res, next) => {
    res.render('index', { title: 'DayLogApp' });
});

module.exports = router;
