'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var UserModel = require('./user.model');

var DayLogSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    slug: {
        type: String,
        required: true,
        unique: true
    },
    location: {
        type: String,
        required: true
    },
    logAt: {
        type: Date,
        required: true
    },
    category: {
        type: String,
        enum: ['Adequate', 'Minor', 'Major'],
        required: true,
    },
    updatedAt: {
        type: Date,
        default: Date.now,
    },
    user: {
        type: ObjectId,
        ref: 'UserModel'
    }
});

var DayLogModel = mongoose.model('DayLog', DayLogSchema);

module.exports = DayLogModel;
